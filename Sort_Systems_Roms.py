#!/usr/bin/python
# encoding: utf-8
from __future__ import unicode_literals, absolute_import

"""
Delete duplicate roms MAME with other systems
"""

__author__ = 'ezechielxae'


import os, sys, re
import xml.etree.ElementTree as ET
import logging, shutil
from configparser import ConfigParser
from logging.handlers import RotatingFileHandler


listSystemSearch = []
cfg = ConfigParser()
cfg.read("init.ini")



nameSystemOutput = cfg.get('Settings', 'Name_System_Output')
pathRomsEntry = cfg.get('Settings', 'Dir_Roms_Entry')
pathRomsOutput = cfg.get('Settings', 'Dir_Roms_Output')
winUnixSlash = cfg.get('Settings', 'Slash_Unix_Or_Win')

#logger = logging.getLogger()
#logger.setLevel(logging.DEBUG)
#file_handler = RotatingFileHandler(pathLog, 'a', 1000000)
#file_handler.setLevel(logging.DEBUG)
#file_handler.setFormatter(logging.Formatter('%(asctime)s :: %(message)s'))
#logger.addHandler(file_handler)


def createDirAndCopyRom(src, dst):
	try :				
		os.mkdir(dst)
		shutil.copy(src, dst)
	except :
		shutil.copy(src, dst)


#Create Dir
try:	
	baseDir = pathRomsOutput+winUnixSlash+nameSystemOutput
	
	os.mkdir(baseDir)
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"0-9"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"ABCD"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"EFGH"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"IJKL"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"MNOP"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"QRST"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"UVW"))
	os.mkdir("%s%s%s" % (baseDir,winUnixSlash,"XYZ"))
except:
	pass


#read Dir entry
for rom in os.listdir(os.path.realpath(pathRomsEntry)):
	if ( rom[0] == "0" or rom[0] == "1" or rom[0] == "2" or rom[0] == "3" 
		or rom[0] == "4" or rom[0] == "5" or rom[0] == "6" or rom[0] == "7"
		or rom[0] == "8" or rom[0] == "9"
	) :
		secondDir = baseDir+winUnixSlash+"0-9"
	elif ( rom[0] == "A" or rom[0] == "B" or rom[0] == "C" or rom[0] == "D" ) :
		secondDir = baseDir+winUnixSlash+"ABCD"
	elif ( rom[0] == "E" or rom[0] == "F" or rom[0] == "G" or rom[0] == "H" ) :
		secondDir = baseDir+winUnixSlash+"EFGH"
	elif ( rom[0] == "I" or rom[0] == "J" or rom[0] == "K" or rom[0] == "L" ) :
		secondDir = baseDir+winUnixSlash+"IJKL"
	elif ( rom[0] == "M" or rom[0] == "N" or rom[0] == "O" or rom[0] == "P" ) :
		secondDir = baseDir+winUnixSlash+"MNOP"
	elif ( rom[0] == "Q" or rom[0] == "R" or rom[0] == "S" or rom[0] == "T" ) :
		secondDir = baseDir+winUnixSlash+"QRST"
	elif ( rom[0] == "U" or rom[0] == "V" or rom[0] == "W" ) :
		secondDir = baseDir+winUnixSlash+"UVW"
	elif ( rom[0] == "X" or rom[0] == "Y" or rom[0] == "Z" ) :
		secondDir = baseDir+winUnixSlash+"XYZ"
		
	#Create Dir AlphaNumeric
	try :
		os.mkdir("%s%s%s" % (secondDir,winUnixSlash,rom[0]))
	except :
		pass
		
	#Game Beta
	if ( re.search(r'(Beta)', rom) or re.search(r'(Sample)', rom) ) is not None:
		#Not Beta Or Sample Games	
		pass	
	else:
		#Game Brazil
		if re.search(r'(Brazil)', rom) is not None:
			dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"Brazil/")
			src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
		
			createDirAndCopyRom(src, dst)				
		#Game unl
		elif re.search(r'(Unl)', rom) is not None:
			dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"Unl/")
			src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
		
			createDirAndCopyRom(src, dst)
		#Game Proto
		elif re.search(r'(Proto)', rom) is not None:
			dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"Proto/")
			src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
		
			createDirAndCopyRom(src, dst)
			
		else:
			#Game PAL
			if ( re.search(r'\(Europe', rom) or re.search(r'\(France', rom) or re.search(r'\(En,Fr', rom) ) is not None:
				dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"PAL/")
				src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
				
				createDirAndCopyRom(src, dst)
			#Game JAPAN
			elif ( re.search(r'\(Japan', rom) or re.search(r'\(Korea', rom) ) is not None:
				dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"JAP/")
				src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
				
				createDirAndCopyRom(src, dst)
			#Game Asia
			elif ( re.search(r'\(Taiwan', rom) or re.search(r'\(Asia', rom)  ) is not None:
				dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"Asia/")
				src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
				
				createDirAndCopyRom(src, dst)
			#Game USA		
			elif ( re.search(r'\(USA', rom) or re.search(r'\(World', rom) ) is not None:
				dst = "%s%s%s%s%s" % (secondDir,winUnixSlash,rom[0],winUnixSlash,"USA/")
				src = "%s%s%s" % (os.path.realpath(pathRomsEntry),winUnixSlash,rom)
				
				createDirAndCopyRom(src, dst)
			